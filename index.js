const readline = require('readline')
const i2cBus = require('i2c-bus')
const Pca9685Driver = require('pca9685').Pca9685Driver

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

const servoSchema = {
  min: 500,
  max: 2500,
  channels: [0, 1, 2, 3]
}

const config = {
  head: 0,
  larm: 1,
  rarm: 2,
  spine: 3,
  lhip: 4,
  rhip: 5
}

const pwm = new Pca9685Driver({
  i2c: i2cBus.openSync(1),
  address: 0x40,
  frequency: 50,
  debug: true
}, (err) => {
  if (err) {
    console.error('Error initializing PCA9685')
    process.exit(-1)
  }

  console.log('Starting servo loop...')
})

rl.on('line', (line) => {
  const number = parseInt(line, 10)

  if (line === 'exit') {
    pwm.dispose()
    process.exit(0)
  } else if (line === 'no') {
    shakeNo()
  }

  if (isNaN(number)) return false

  if (number >= servoSchema.min && number <= servoSchema.max) {
    setPulseLengths(servoSchema.channels, number)
  }
})

const setPulseLengths = (indexes, microseconds) => {
  indexes.forEach(index => {
    pwm.setPulseLength(index, microseconds)
  })
}

const setAngleAbsolute = (index, distance) => {
  const pulseTotal = servoSchema.max - servoSchema.min

  pwm.setPulseLength(index, servoSchema.min + Math.floor(pulseTotal / 100 * distance))
}

const shakeNo = () => {
  return new Promise(resolve => {
    // center
    setAngleAbsolute(config.head, 50)

    // turn left
    setTimeout(() => {
      setAngleAbsolute(config.head, 25)

      // turn right
      setTimeout(() => {
        setAngleAbsolute(config.head, 75)

        // center
        setTimeout(() => {
          setAngleAbsolute(config.head, 50)

          resolve()
        }, 500)
      }, 1000)
    }, 500)
  })
}
