include <../variables.scad>;

use <../mg90s.scad>;
use <../mg90s-single-arm.scad>;

armOffset = mainBoxLength/2 + 4;
armOffsetZ = 8.5;
hipWingOffset = 2;
neckOffset = wingTopOffset + wingBladeHeight;

module torso() {
    translate([0, 0, (servoMountingHeight - neckOffset - hipWingOffset - hexScrewHeadHeight)/2])
        cube([
            screwPlaneWidth,
            (armOffset + servoMountingHeight)*2,
            servoMountingHeight + neckOffset + hipWingOffset + hexScrewHeadHeight
        ], center = true);
}

module neck() {
    servo(centerX = true, centerY = true, holes = false, arm = "single-arm", threading = "down", cableAngle = 45);
}

module hip_wing () {
    translate([0, 0, -(hipWingOffset + hexScrewHeadHeight)])
        rotate([180, 0, 0])
            single_arm(threading = "up", centerX = true);
}

module left_arm() {
    translate([0, -armOffset, armOffsetZ])
        rotate([90, 0, 0])
            servo(centerX = true, centerY = true, holes = false, arm = "single-arm", threading = "down", cableAngle = 45);
}

module right_arm() {
    mirror([0, 1, 0]) left_arm();
}

module spine() {
    mirror([0, 0, 1]) neck();
}


difference() {
    torso();

    neck();
    hip_wing();
    left_arm();
    right_arm();

    // connect left and right arm with hollow cutout
    translate([0, 0, armOffsetZ])
        rotate([90, 0, 0])
            cube([mainBoxWidth, mainBoxLength, armOffset * 2], center = true);
}
