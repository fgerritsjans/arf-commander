use <../mg90s-single-arm.scad>;

headLength = 26;

module robot_head_eye() {
    $fn = 32;
    translate([headLength/2, 3, 6])
        sphere(2.5);
}

difference() {
    rotate([180, 0, 0]) {
        difference() {
            translate([0, 0, -6])
                cube([headLength, headLength / 2, 12], center = true);

            translate([0, 0, -2])
                single_arm(threading = "up", centerX = true);
        }
    }

    robot_head_eye();

    mirror([0, 1, 0])
        robot_head_eye();
}




