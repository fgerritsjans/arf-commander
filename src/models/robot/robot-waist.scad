include <../variables.scad>;

use <../mg90s.scad>;
use <../mg90s-single-arm.scad>;

waistHeight = servoMountingHeight + screwPlaneHeight*2;
waistSpacing = 2;

module waist() {
    translate([0, 0, waistHeight/2 - screwPlaneHeight])
        cube([screwPlaneWidth, mainBoxLength*3 + waistSpacing*4, waistHeight], center = true);
}

module spine() {
    translate([0, 0, 0])
        servo(centerX = true, centerY = true, holes = false, arm = "single-arm", threading = "down", cableAngle = 90);
}

module left_pelvis() {
    translate([0, mainBoxLength + 2, servoMountingHeight]) {
        rotate([0, 180, 0])
            servo(centerX = true, centerY = true, holes = false, arm = "single-arm", threading = "down");

        cableThickness = 1.4;

        translate([-mainBoxWidth/2, -servoConnectorWidth/2, 0]) {
            cube([mainBoxWidth, servoConnectorWidth, cableThickness]); // space under servo

            translate([-10, 0, -servoConnectorHeight + cableThickness])
                cube([10, servoConnectorWidth, servoConnectorHeight]);  // space behind the servo as exit
        }
    }
}

module right_pelvis() {
    mirror([0, 1, 0]) left_pelvis();
}

difference() {
    waist();

    spine();
    left_pelvis();
    right_pelvis();
}

