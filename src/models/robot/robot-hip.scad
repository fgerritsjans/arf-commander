include <../variables.scad>;
use <../mg90s.scad>;
use <../mg90s-gear.scad>;

hipWidth = wingCylinderInsertRadius + 2;
hipWidthBottom = hipWidth + 1;
hipLength = mainBoxHeightMotor + (mainBoxBladeHeight*2);
hipThickness = hipLength - mainBoxHeightMotor;

localCylinderOffsetZ = mainBoxLength*1.5;
localCylinderOffsetX = 5;

module robot_hip () {
    difference() {
        hull() {
            localCubeHeight = 0.5;
            translate([0, 0, - localCubeHeight / 2])
                cube([hipWidth, hipLength, localCubeHeight], center = true);

            translate([localCylinderOffsetX, 0, - localCylinderOffsetZ])
                rotate([90, 0, 0])
                    cylinder(r = hipWidthBottom / 2, h = hipLength, $fn = circleFn, center = true);
        }

        localMountingBlock();

        translate([0, motorMainGearOffset, 0])
            rotate([180, 0, 0])
                gear(screw = true);

        servoPlacement();
        servoGearCutout();
    }
}

%servoPlacement();
%servoGearCutout();

//translate([0, -mainBoxHeightMotor/2, 0])
//    cube([1, mainBoxHeightArmInsert, 1]);

module localMountingBlock () {
    color("Red") {
        localCubeHeight = localCylinderOffsetZ + (hipWidth/2) - gearCutoutDepth;
        translate([
            -hipWidth/2,
            -mainBoxHeightMotor/2,
            -(gearCutoutDepth + localCubeHeight)
        ])
            cube([
                hipWidth + localCylinderOffsetX,
                mainBoxHeightMotor,
                localCubeHeight
            ]);
    }
}

module servoPlacement () {
    // calculate the arm angle
    // * using the cylinder cutout location as reference for the atan2 math
    // * no delta calculation is done as the location to point to is 0,0
    localArmAngle = 90 + atan2(localCylinderOffsetX, localCylinderOffsetZ);

    // todo: cutout secondary servo for multi axis movement
    translate([localCylinderOffsetX, -motorCircleHeight/2, -localCylinderOffsetZ])
        rotate([-90, 0, 0])
            translate([motorMainGearOffset, 0, 0])
                servo(
                centerX = true,
                centerY = true,
                centerZ = true,
                holes = false,
                arm = "single-arm",
                armAngle = -(localArmAngle)
            );
}

module servoGearCutout () {
    // adding 1 height due to a rendering clipping issue
    translate([
        localCylinderOffsetX,
        mainBoxHeightMotor/2 - 1,
        -localCylinderOffsetZ
    ]) {
        translate([0, 0, -gearMainDiameter/2])
            cube([hipWidthBottom, gearHeight + 1, gearMainDiameter]);
    }
}

robot_hip();
//mirror([1,0,0]) robot_hip();
