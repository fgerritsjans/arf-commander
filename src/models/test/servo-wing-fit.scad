use <../mg90s.scad>;
use <../mg90s-single-arm.scad>;

%difference() {
    translate([1.8, 0, 31.8])
        cube([26, 11, 3], center = true);

    servo(centerY = true, centerX = true, arm = "single-arm");
}

difference() {
    translate([0, 0, 1.5])
        cube([26, 10, 3], center = true);

    translate([0, 0, 1])
        single_arm(threading = "up", centerX = true);
}
