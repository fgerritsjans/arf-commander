use <../mg90s.scad>;

difference () {
    cubeHeight = 6.5;

    translate([0, 0, cubeHeight / 2 - 0.5])
        cube([35, 40, cubeHeight], center = true);

    rotate([90, 0, 0])
        servo(centerX = true, centerY = false, centerZ = true, holes = false, centerAbsolute = true, arm = "single-arm");

    // servo cutout example
    translate([0, 5, 0])
        cylinder(10, r1 = 10, d2 = 10, center = true, $fn = 25);
    translate([-19, 13.15, 1])
        cube([10, 3, 6]);
}

