include <../variables.scad>;
use <../mg90s-gear.scad>;

difference() {
    wallThickness = 1;
    cylinder(r = (gearMaxDiameter/2) + 2, h = gearHeight+wallThickness, $fn = 40);

    translate([0, 0, wallThickness]) gear();
};


