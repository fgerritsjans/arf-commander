// defines the smoothness for circles
circleFn = 60;

// thickness between the screw and the servo
screwPlate = 2;

// for tight/difficult print parts we have a margin available
printMargin = 0.4;

// hex nuts
hexNutDiameter = 2.5;
hexNutHeight = 2;
hexNutClearance = hexNutHeight * 2;

hexScrewHeadHeight = hexNutHeight;

// servo mount plane
screwPlaneWidth = 32.2;
screwPlaneHeight = 2.4;
screwHoleWidth = hexNutDiameter + printMargin;

distanceApart = 25;

// servo dimensions
mainBoxWidth = 23;
mainBoxHeight = 23.8;
mainBoxLength = 12.5 + printMargin;

// casing for the biggest gear
motorCircleHeight = 4.5;
motorCircleDiameter = 11.6;
motorMainGearHeight = 4;
motorMainGearOffset = motorCircleDiameter / 2;

// casing for the smaller gear
motorSecondaryCircleDiameter = 5.4;
motorSecondaryGearOffset = 14.5 - (motorSecondaryCircleDiameter / 2);

// servo wing
wingBladeHeight = 2.1;
wingCenterHeight = 4.6;
wingScrewClearance = 10;
wingTopOffset = 0.2;
wingCylinderRadius = 4.3;
wingCylinderInsertRadius = 7.1; // 6.8

// servo
servoMountingHeight = 17;

// servo wiring
servoConnectorWidth = 7.6 + printMargin*2;
servoConnectorHeight = 2.6 + printMargin*2;
servoConnectorCableLength = 10;

// servo gear specifications
gearSections = 20;
gearMinDiameter = 4.4 + printMargin*1.75;
gearMaxDiameter = 4.8 + printMargin*1.75;
gearMainDiameter = 4.7;
gearHeight = 3.5;

// easy variables for miscellaneous use
mainBoxHeightMotor = mainBoxHeight + motorCircleHeight;
mainBoxHeightArm = mainBoxHeightMotor + motorMainGearHeight + 1.7; // this is the total servo height with the arm attached
mainBoxHeightArmInsert = mainBoxHeightArm - wingTopOffset;
mainBoxBladeHeight = mainBoxHeightArmInsert - mainBoxHeightMotor;
gearCutoutDepth = gearHeight + screwPlate + hexNutHeight;

