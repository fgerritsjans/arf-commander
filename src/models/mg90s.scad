include <variables.scad>;
use <mg90s-single-arm.scad>;
use <mg90s-gear.scad>;

module servoCutOut(punctureHeight = 1) {
    translate([(distanceApart / 2) + (screwHoleWidth / 2), 0, 0])
        cylinder(r = screwHoleWidth / 2, h = punctureHeight, center = true, $fn = 45);

    // cutout next to threading
    gapWidth = 1.2;
    gapLength = 4;

    translate([distanceApart/2 + screwHoleWidth / 2 + gapLength / 2, 0, 0])
        cube([gapLength, gapWidth, punctureHeight], center = true);
}

module servoThreading(threading, threadingDepth) {
    threadingPosZ = -threadingDepth + screwPlaneHeight / 2;

    rotate([threading == "up" ? 180 : 0, 0, 0])
        translate([distanceApart / 2 + screwHoleWidth / 2, 0, threadingPosZ])
        union() {
            cylinder(r = screwHoleWidth / 2, h = threadingDepth, $fn = 45);
            rotate([0, 0, 360 / 12])
                cylinder(r = screwHoleWidth, h = hexNutHeight, $fn = 6);

            translate([-screwHoleWidth, -screwHoleWidth, -hexNutClearance])
                cube([screwHoleWidth*2, screwHoleWidth*2, hexNutClearance]);
        }
}

module servo(
    center = false,
    centerX = false,
    centerY = false,
    centerZ = false,
    centerAbsolute = false,
    holes = true,
    arm = false,
    armAngle = 0,
    threading = false,
    threadingDepth = 7.5,
    cableAngle = false,
    cableDepth = false,
    gear = false
) {
    centerHeight = centerAbsolute
        ? mainBoxHeight + motorCircleHeight + motorMainGearHeight
        : mainBoxHeight;

    translate([
        centerX || center ? -mainBoxWidth/2 : 0,
        centerY || center ? -mainBoxLength/2 : 0,
        centerZ || center ? -centerHeight/2 : 0
    ]) {
        color("LightBlue", 0.5) {
            $fn = 45;

            // servo case
            cube([mainBoxWidth, mainBoxLength, mainBoxHeight]);

            translate([
                mainBoxWidth / 2,
                mainBoxLength / 2,
                servoMountingHeight + (screwPlaneHeight / 2)
            ]) difference() {
                union() {
                    cube([screwPlaneWidth, mainBoxLength, screwPlaneHeight], center = true);

                    if (threading) {
                        servoThreading(threading, threadingDepth);
                        mirror([1,0,0]) servoThreading(threading, threadingDepth);
                    }
                }

                cutOutHeight = screwPlaneHeight + 0.2;

                if (holes) {
                    servoCutOut(cutOutHeight);
                    mirror([1,0,0]) servoCutOut(cutOutHeight);
                }
            }

            // main gear casing
            translate([motorMainGearOffset, mainBoxLength / 2, mainBoxHeight])
                cylinder(r = motorCircleDiameter / 2, h = motorCircleHeight);

            // secondary gear casing
            translate([
                motorSecondaryGearOffset,
                mainBoxLength / 2,
                mainBoxHeight
            ]) cylinder(r = motorSecondaryCircleDiameter / 2, h = motorCircleHeight);

            // exposed main gear
            translate([motorMainGearOffset, mainBoxLength / 2, mainBoxHeight + motorCircleHeight]) {
                if (gear) {
                    gear(screw = true);
                } else {
                    cylinder(r = gearMainDiameter / 2, h = motorMainGearHeight);
                }
            }

            if (arm == "single-arm") {
                translate([motorMainGearOffset, mainBoxLength/2, mainBoxHeightArm])
                    rotate([180, 0, armAngle])
                        single_arm();
            }
        }

        if (cableAngle != false) {
            color("LightCoral") {
                depth = cableDepth ? cableDepth : servoConnectorCableLength;

                translate([
                    servoConnectorHeight/2,
                    mainBoxLength/2,
                    servoConnectorHeight/2
                ])

                    rotate([0, cableAngle, 0])
                    translate([0, 0, -depth/2]) // change the rotation axis
                    cube([
                        servoConnectorHeight,
                        servoConnectorWidth,
                        depth + servoConnectorHeight
                    ], center = true);
            }
        }
    }
}

servo();

translate([0, 25, 0])
    servo(holes = false, threading = "down", cableAngle = 45);

translate([40, 0, 0])
    servo(arm = "single-arm");

translate([40, 25, 0])
    servo(arm = "single-arm", armAngle = 90, holes = false, threading = "up", threadingDepth = 6);

translate([0, 50, 0])
    servo(holes = false, threading = "down", cableAngle = 45, gear = true);
