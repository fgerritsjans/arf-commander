include <variables.scad>;

module single_arm_threading (direction, depth) {
    color("LightCoral", 0.5) {
        translate([0, 0, direction == "up" ? -depth : 0]) {
            cylinder(h = wingCenterHeight + depth, r = screwHoleWidth / 2);

            translate([0, 0, direction == "up" ? -wingScrewClearance : depth + wingCenterHeight])
                cylinder(r = screwHoleWidth, h = wingScrewClearance);
        }

    }
}

module single_arm (threading = false, threadingDepth = 2, center = false, centerX = false) {
    $fn = 64;

    wingSpan = 21.9; // 21.3
    wingCylinderOffset = wingSpan - (wingCylinderInsertRadius / 2) - (wingCylinderRadius / 2);

    translate([
        centerX || center ? wingCylinderInsertRadius / 2 - (wingSpan/2) : 0,
        0,
        0
    ]) union() {
        color("LightBlue", 0.5) {
            cylinder(h = wingCenterHeight, r = wingCylinderInsertRadius / 2);

            hull() {
                translate([0, 0, wingTopOffset])
                    cylinder(h = wingBladeHeight, r = wingCylinderInsertRadius / 2);

                translate([wingCylinderOffset, 0, wingTopOffset])
                    cylinder(h = wingBladeHeight, r = wingCylinderRadius / 2);
            }
        }

        if (threading) {
            single_arm_threading(direction = threading, depth = threadingDepth);
        }
    }
}

single_arm(centerX = true);

translate([0, 10, 0])
    single_arm(threading = "up", threadingDepth = 4);

translate([0, 20, 0])
    single_arm(threading = "down", threadingDepth = 2);
