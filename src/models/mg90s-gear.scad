include <variables.scad>;

function getPositionForCircle(diameter, angle) = [
    cos(angle) * (diameter/2),
    sin(angle) * (diameter/2)
];

gearPolygons = [
    for (i = [0 : (gearSections*2) - 1])
        getPositionForCircle(
            i % 2 ? gearMinDiameter : gearMaxDiameter,
            360 / (gearSections*2) * i
        )
];

module gear(screw = false) {
    linear_extrude(height = gearHeight) {
        polygon(gearPolygons);
    }

    if (screw) {
        // hole for head of the screw
        translate([0, 0, gearHeight])
            cylinder(h = screwPlate, r = screwHoleWidth / 2, $fn = circleFn);

        // ensure 20mm clearance for a screwdriver
        translate([0, 0, gearHeight + screwPlate])
            cylinder(h = 20, r = screwHoleWidth, $fn = circleFn);
    }
}

module gear_2d() {
    polygon(gearPolygons);
}

gear();

translate([10, 0, 0]) gear(screw = true);
