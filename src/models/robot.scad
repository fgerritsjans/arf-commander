use <mg90s.scad>;

module servo_placement() {
    translate([-5, 0, 0])
        rotate([180, 0, 180])
        servo(holes = false, threading = "down", arm = "single-arm", centerY = true);
}

servoOffset = 17;

translate([0, 0, servoOffset])
    difference() {
        translate([-34, -9, -servoOffset]) cube([68, 18, 5]);
        servo_placement();
        mirror([-1, 0, 0]) servo_placement();
    }

// rotate([180, 0, 90]) servo(holes = false, threading = "down", arm = "single-arm", centerX = true, centerY = true);
