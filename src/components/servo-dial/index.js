class ServoDial extends HTMLElement {
  static get observedAttributes () { return ['onChange'] }

  constructor () {
    super()

    const shadow = this.attachShadow({ mode: 'open' })

    const label = document.createElement('slot')
    label.setAttribute('class', 'label')

    shadow.appendChild(label)
  }
}

export default ServoDial
